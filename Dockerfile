FROM "registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.17.0-kube-1.15.12-alpine-3.12"

COPY src/ build/

RUN wget https://github.com/roboll/helmfile/releases/download/v0.120.0/helmfile_linux_amd64 \
  && mv helmfile_linux_amd64 /usr/local/bin/helmfile \
  && chmod u+x /usr/local/bin/helmfile

RUN apk add --no-cache bash

RUN helm init --client-only \
  && helm plugin install https://github.com/databus23/helm-diff \
  && helm plugin install https://github.com/aslafy-z/helm-git.git --version 0.8.1

RUN ln -s /build/bin/* /usr/local/bin/ \
  && ln -s /build/default-data /usr/local/share/gitlab-managed-apps

# Keep SSOT for the applications' values.yml. We do this for these values which
# are too complex to keep track of manually. For simple values.yml, we want to check in
# the files so that we can review changes.
# We should remove this once CI Managed Apps substitute non-CI managed apps.
# Then these files should be in the cluster-applications project.
RUN wget https://gitlab.com/gitlab-org/gitlab/-/raw/master/vendor/prometheus/values.yaml \
      -O /build/default-data/prometheus/values.yaml
RUN wget https://gitlab.com/gitlab-org/gitlab/-/raw/master/vendor/elastic_stack/values.yaml \
      -O /build/default-data/elastic-stack/values.yaml
RUN wget https://gitlab.com/gitlab-org/gitlab/-/raw/master/vendor/ingress/modsecurity.conf \
      -O /build/default-data/ingress/modsecurity.conf
#-------------------------------------------------------------------------------
