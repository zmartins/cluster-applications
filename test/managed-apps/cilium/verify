#!/bin/bash -e

function ping_nginx {
  kubectl run "$1" \
    -n policy-test \
    --generator run-pod/v1 \
    --image busybox \
    --rm -ti --restart Never \
    --command -- wget -q --timeout=5 nginx -O -
}

function cleanup {
  echo "Removing policy-test namespace..."
  kubectl delete namespace policy-test > /dev/null 2>&1 || true
}

function fail {
  echo "$1"
  cleanup
  exit 1
}

# Only do tests on GKE, k3s service is running on port 6443
if kubectl cluster-info | grep -q :6443 ; then
  echo "k3s cluster detected, exiting..."
  exit 0
fi

echo "Verifying hubble-relay..."
if ! kubectl get svc -n "$TILLER_NAMESPACE" hubble-relay > /dev/null 2>&1; then
  fail "Failed to install hubble-relay service"
fi

echo "Setting up test environment..."
if ! kubectl get namespace policy-test > /dev/null 2>&1; then
  kubectl create namespace policy-test
  kubectl run -n policy-test --generator run-pod/v1 --image nginx nginx
  kubectl wait -n policy-test --for condition=Ready --timeout 60s pods/nginx
  kubectl expose -n policy-test pod nginx --port 80
  kubectl create -f - <<EOF
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: access-nginx
  namespace: policy-test
spec:
  podSelector:
    matchLabels:
      run: nginx
  ingress:
    - from:
      - podSelector:
          matchLabels:
            run: access
EOF
fi

echo "Verifying access..."
if ! ping_nginx access; then
  fail "Unable to ping from the whitelisted pod"
fi

echo "Verifying drops..."
if [ "$1" == "audit" ]; then
  if ! ping_nginx drop; then
    fail "Unable to ping from the non-whitelisted pod in audit mode"
  fi
else
  if ping_nginx drop; then
    fail "Able to ping from the non-whitelisted pod"
  fi
fi

cleanup
