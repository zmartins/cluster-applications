#!/bin/bash -eu

MAX_RETRIES=30

function falco_log_entries {
  for i in $(seq $MAX_RETRIES)
  do
    if kubectl --namespace "$TILLER_NAMESPACE" logs --selector app=falco --tail=-1 | grep -Fo "$1" | wc -l | xargs -I % test % -ge "${2:-1}"; then
      sleep 10
      return 0
    fi
    echo "$i: Log entry ($1) not found in falco logs, retrying in 10 seconds..."
    sleep 10
  done
  return 1
}

function cleanup {
  echo "Removing falco-test namespace..."
  kubectl delete namespace falco-test > /dev/null 2>&1 || true
}

function fail {
  echo "$1"
  cleanup
  exit 1
}

echo "Verifying falco..."

if ! kubectl get daemonset --namespace "$TILLER_NAMESPACE" falco; then
  fail "Could not find falco deployment."
fi

FALCO_PODS_COUNT=$(kubectl get daemonset --namespace "$TILLER_NAMESPACE" falco -o jsonpath="{.status.desiredNumberScheduled}")

echo "Checking if falco has started..."
if ! falco_log_entries "Starting internal webserver" "$FALCO_PODS_COUNT"; then
  fail "Falco was not able to start within 5 minutes"
fi

echo "Setting up test environment..."
if ! kubectl get namespace falco-test > /dev/null 2>&1; then
  kubectl create namespace falco-test
  kubectl run --namespace falco-test --generator run-pod/v1 --image nginx nginx
  kubectl wait --namespace falco-test --for condition=Ready --timeout 60s pods/nginx
fi

echo "Verifying alerts..."

ABUSIVE_COMMANDS=(
  "cp /etc/passwd /root/passwd"
  "touch /bin/new-binary"
  "mkdir -p /bin/new-bin-directory/new-binary"
  "echo 'TEST' >> /etc/test"
  "cp /etc/shadow /root/shadow"
  "mkdir -p /bin/another-new-bin-directory/new-binary-directory"
  "touch /bin/another-new-binary"
  "echo 'FALCO' >> /etc/falco.test"
)

for command in "${ABUSIVE_COMMANDS[@]}"; do
  kubectl exec nginx --namespace falco-test -- bash -c "$command"
  sleep 10
done

ABUSED_RULES=("Write below root" "Write below binary dir" "Mkdir binary dirs")
for rule in "${ABUSED_RULES[@]}"; do
  if ! falco_log_entries "$rule"; then
    kubectl --namespace "$TILLER_NAMESPACE" logs --selector app=falco --tail=-1
    echo "Event ($rule) not captured by falco"
  fi
done

cleanup
